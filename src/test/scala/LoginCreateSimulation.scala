import Config.users
import io.gatling.core.Predef.{atOnceUsers, openInjectionProfileFactory}
import io.gatling.core.scenario.Simulation

class LoginCreateSimulation extends Simulation{

    private val loginUserExec = LoginCreateScenario.loginCreateUserScenario
      .inject(atOnceUsers(users))

    setUp(loginUserExec)

}
